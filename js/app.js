var infos={};
var pages=1;
var page=1;
var c;
var input;
function showModal(id) {
    $('#myModal').modal('show');
    $('#popup-img').attr('src', $("#img"+id).attr('src'));
    $('#title').text(infos[id].title);
    $('#date').text(infos[id].date);
    $('#realname').text(infos[id].realname);

}
function info(id ,secret){
    if(infos[id]==undefined) {
        $.get("https://www.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=af7f3a84c0766abfe01b30d63e917b36&photo_id=" + id + "&secret=" + secret + "&format=json&nojsoncallback=1", function (info) {
            infos[id]={
                'title': info.photo.title._content,
                'date': info.photo.dates.taken, 
                'realname': info.photo.owner.realname,
            };
            showModal(id);
        });
    } else {
        showModal(id);
    }
}
function getPhliker(){
    console.log("getPhliker");
    if(page<=pages){
        $.get("https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=af7f3a84c0766abfe01b30d63e917b36&format=json&nojsoncallback=1&page="+page+"&per_page=24&tags=" + input, function (data) {
            page++;
            c = data.photos.photo;
            pages=data.photos.pages;
            console.log(c);
            var row = $("<div class='row'></div>");
            for (var i = 0; i < c.length; i++) {
                var url = "https://farm" + c[i].farm + ".staticflickr.com/" + c[i].server + "/" + c[i].id + "_" + c[i].secret + ".jpg";
                row.append('<div class="col"><img id="img'+c[i].id+'"  class="rounded" onclick="info(\''+c[i].id+'\', \''+c[i].secret+'\')" src=' + url + ' height=100 width=100  style="margin:0.1%; margin-top:10px"></div>');
                if (i % 8 == 7) {
                    $('#images').append(row);
                    row = $("<div class='row'></div>");
                }
            } 
            $("#continue").parent().removeClass("d-none");   
        })
    }
}
$(document).ready(function () {
    $('#search-btn').click(function () {
        $('#search-hide').hide();
        $('#images').html("");
        input = $('#search').val();
        getPhliker();
    });
    $('#btnsearch').click(function () {
        $('#search-hide').hide();
        $('#images').html("");
        input = $('#insearch').val();
        getPhliker();
    });
  
    $("#continue").click(function () {
        getPhliker();
    });
});


  
      